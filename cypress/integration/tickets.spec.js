const { cyan } = require("color-name");
const { and } = require("ramda");

    describe("Tickets", () => {
        beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

        it("campos do tipo texto",() =>{
            const firstName = "Daniel";
            const lastName = "Santos";

            cy.get("#first-name").type(firstName);
            cy.get("#last-name").type(lastName);
            cy.get("#email").type("teste@teste.com.br");
            cy.get("#requests").type("Carnívoro");
            cy.get("#signature").type(`${firstName} && ${lastName}`);
        });

        it("selecionar dois tickets", () =>{
            cy.get("#ticket-quantity").select("2")
        })

        it("Selecionar radio buttons", ()=>{
            cy.get("#vip").check();
        });

        it("Integarindo com checkboxes",() =>{
            cy.get("#friend").check();
            cy.get("#friend").uncheck();
            cy.get("#social-media").check();
            cy.get("#publication").check();
        });
     
        it("Visitando a página de teste e validando o texto", () =>{
            cy.get("header h1").should("contain", "TICKETBOX")
        });

        it("Alertando quando o e-mail está inválido", () => {

            cy.get("#email").as("email").type("teste-teste.com.br");
            cy.get("#email.invalid").should("exist");
            cy.get("@email").clear().type("teste@teste.com.br")
            cy.get("#email.invalid").should("not.exist");
        });
        
        it("Preenche e reinicia o formulário", () => {
            const firstName = "Daniel";
            const lastName = "Santos";
            const fullName = `${firstName} ${lastName}`;

            cy.get("#first-name").type(firstName);
            cy.get("#last-name").type(lastName);
            cy.get("#email").type("teste@teste.com.br");
            cy.get("#ticket-quantity").select("2");
            cy.get("#vip").check();
            cy.get("#friend").check();
            cy.get("#requests").type("Carnívoro");
            cy.get(".agreement p").should("contain", 
            `I, ${fullName}, wish to buy 2 VIP tickets.`);
            cy.get("#agree").click();
            cy.get("#signature").type(`${firstName} ${lastName}`);
            cy.get("button[type='submit']").as("submitButton").should("not.be.disabled");
            cy.get("button[type='reset']").click();
            cy.get("@submitButton").should("be.disabled");

        });

        it("Preenche e verificar campos obrigatórios", () => {
            const customer = {
                firstName: "João",
                lastName: "Silva",
                email: "joãosilva@example.com.br",
            };
            cy.camposObrigatorios(customer);
            cy.get("button[type='submit']").as("submitButton").should("not.be.disabled");
            cy.get("#agree").uncheck();
            cy.get("@submitButton").should("be.disabled");

        });




    });